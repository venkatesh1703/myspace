package com.innominds.myspace.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * Enity class for Successful projects
 * @author Nilesh
 *
 */
@Entity
@Table( name = "successfull_projects")
public class SuccessfullProjects {
	@Id
	@Column(name = "project_name")
	private String project_name;
	
	@Column(name= "description")
	private String description;
	
	@Column(name = "completion_date")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date completion_date;
	
	@Column(name = "project_head_id")
	private int project_head_id;
	
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCompletion_date() {
		return completion_date;
	}
	public void setCompletion_date(Date completion_date) {
		this.completion_date = completion_date;
	}
	public int getProject_head_id() {
		return project_head_id;
	}
	public void setProject_head_id(int project_head_id) {
		this.project_head_id = project_head_id;
	}
	
	public SuccessfullProjects(String project_name, String description, Date completion_date, int project_head_id) {
		super();
		this.project_name = project_name;
		this.description = description;
		this.completion_date = completion_date;
		this.project_head_id = project_head_id;
	}
	public SuccessfullProjects() {
		super();
	}
	

}
