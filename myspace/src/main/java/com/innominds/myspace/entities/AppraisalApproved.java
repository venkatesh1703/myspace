package com.innominds.myspace.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * This is entity class for approved the requested appraisal
 * @author MySpace Batch-A
 *
 */
@Entity
@Table(name="appraisal_approved")
public class AppraisalApproved {

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="appraisal_id")
	private int appraisalId;
	
	@Column(name="employee_id")
	private int employeeId;
	
	@Column(name="status")
	private String ApprovalStatus;
	
	@Column(name="approval_date")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date approvalDate;
	
	@Column(name="approved_by")
	private int approvedBy;
	
	@Column(name="performance_points")
	private int performancePoints;

	//getters and Setters for appraisal approved
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAppraisalId() {
		return appraisalId;
	}

	public void setAppraisalId(int appraisalId) {
		this.appraisalId = appraisalId;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	
	public String getApprovalStatus() {
		return ApprovalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		ApprovalStatus = approvalStatus;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public int getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(int approvedBy) {
		this.approvedBy = approvedBy;
	}

	public int getPerformancePoints() {
		return performancePoints;
	}

	public void setPerformancePoints(int performancePoints) {
		this.performancePoints = performancePoints;
	}
	
}
