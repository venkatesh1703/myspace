package com.innominds.myspace.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * entity class for Question table
 * 
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "question_tbl")
public class Question {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@Id
	private int questionId;

	@Column(name = "posted_by")
	private int postedBy;

	@Column(name = "question")
	private String question;

	@Column(name = "posted_at")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",timezone="GMT+5:30")
	private Date postedAt;

	public Question() {
		super();

	}

	public Question(int questionId, int postedBy, String question, Date postedAt) {
		super();
		this.questionId = questionId;
		this.postedBy = postedBy;
		this.question = question;
		this.postedAt = postedAt;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public int getPostedBy() {
		return postedBy;
	}

	public void setPostedBy(int postedBy) {
		this.postedBy = postedBy;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Date getPostedAt() {
		return postedAt;
	}

	public void setPostedAt(Date postedAt) {
		this.postedAt = postedAt;
	}

}
