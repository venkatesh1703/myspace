package com.innominds.myspace.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.innominds.myspace.entities.Drives;

public interface DriveDao extends JpaRepository<Drives, Integer> {

	/**
	 * Query for getting upcoming drive
	 * 
	 * @param date
	 * @return
	 */
	@Query("FROM Drives WHERE driveDate >=:date")
	public List<Drives> getAllDrives(Date date);

}
