package com.innominds.myspace.dao;

import org.springframework.data.repository.CrudRepository;

import com.innominds.myspace.entities.EmployeeDetails;

/**
 * Dao clas for search the employee by EmployeeName if the employee exist with
 * that name then it will return Employee object else it returns null
 * 
 * @author IMVIZAG
 *
 */
public interface SearchEmployeeDao extends CrudRepository<EmployeeDetails, Integer> {
	/**
	 * This method is for search the employee based on the employee name if the
	 * employee exist it will returns the employee object else it will return the
	 * empty object
	 * 
	 * @param name
	 * @return
	 */
	public EmployeeDetails findByname(String name);

}
