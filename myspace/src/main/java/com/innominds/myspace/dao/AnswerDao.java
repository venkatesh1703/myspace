package com.innominds.myspace.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.innominds.myspace.entities.Answer;

/**
 * This interface extends CrudRepository. So, we can perform all CRUD operations
 * on respective table.
 * 
 * @author venkatesh
 *
 */
public interface AnswerDao extends CrudRepository<Answer, Integer> {
	List<Answer> findAllByQuestionId(int questionId);
	boolean existsByQuestionId(int questionId);
}
