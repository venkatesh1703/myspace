package com.innominds.myspace.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.innominds.myspace.entities.EmployeeDetails;

/**
 * Dao interface performs CRUD operation on employee object 
 * @author IMVIZAG
 */
@Repository
public interface EmployeeDao extends CrudRepository<EmployeeDetails, Integer> {

}
