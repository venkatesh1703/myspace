package com.innominds.myspace.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.innominds.myspace.entities.AppraisalRequest;

public interface AppraisalRequestDao extends JpaRepository<AppraisalRequest, Integer> {

	public AppraisalRequest findByEmployeeId(int employeeId);
}
