package com.innominds.myspace.dao;

import org.springframework.data.repository.CrudRepository;

import com.innominds.myspace.entities.SuccessfullProjects;

/**
 * DAO interface for get all the projects fro mthe database
 * 
 * @author IMVIZAG
 *
 */
public interface SuccessfullProjectsDao extends CrudRepository<SuccessfullProjects, Integer>{

}
