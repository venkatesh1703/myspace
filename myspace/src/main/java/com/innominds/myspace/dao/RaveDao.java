package com.innominds.myspace.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.innominds.myspace.entities.Rave;

/**
 * Rave DAO for getting Rave data from the Database;
 * 
 * @author IMVIZAG
 *
 */
public interface RaveDao extends CrudRepository<Rave, Integer> {
	public List<Rave> findByemployeeId(int employeeId);

	public List<Rave> findBycreatedBy(int createdBy);

}
