package com.innominds.myspace.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.innominds.myspace.entities.LoginCredentials;

/**
 * Dao interface performs CRUD operation on Login object
 * 
 * @author IMVIZAG
 *
 */
public interface LoginCredentialsDao extends CrudRepository<LoginCredentials, Integer> {
	
	public LoginCredentials findByUsername(String username);

	@Query("from LoginCredentials where employeeId =:employeeId")
	public LoginCredentials findByid(int employeeId);

}
