package com.innominds.myspace.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innominds.myspace.entities.AppraisalApproved;
import com.innominds.myspace.entities.AppraisalRequest;
import com.innominds.myspace.entities.Drives;
import com.innominds.myspace.entities.EmployeeDetails;
import com.innominds.myspace.entities.FindEmployee;
import com.innominds.myspace.entities.LoginCredentials;
import com.innominds.myspace.entities.SuccessfullProjects;
import com.innominds.myspace.payload.UploadFileResponse;
import com.innominds.myspace.service.EmployeeService;
import com.innominds.myspace.util.JwtImpl;
import com.innominds.myspace.util.TokenTimeOut;
import com.innominds.myspace.util.Validations;

/**
 * This class contains web resource end points which are process the employee
 * requests.
 * 
 * @author Myspace Team-A
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/myspace")
public class EmployeeControllerImpl {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private Validations validations;

	/**
	 * This web resource handles Login request from client. if authentication
	 * success it returns employee id and token. if user name wrong returns status
	 * like wrong user name. if password wrong returns status like wrong password.
	 * 
	 * @param login is LoginCredentials Object reference which contains user name
	 *              and password
	 * @return ResponseEntity
	 */

	@CrossOrigin
	@PostMapping("/login")
	public ResponseEntity<?> doAuthenticate(@RequestBody LoginCredentials login) {
		// calling service method
		int empId = employeeService.doLogin(login.getUsername(), login.getPassword());

		if (empId == -1 || empId ==-2) {
			// if user name wrong it returns 202 status code along with message
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"wrong credentials\",\"statuscode\":" + 202 + ",\"attemptsleft\":"+-empId+"}");
		} else if (empId == -3) {
			// if password is wrong it returns 201 status code along with message
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"you are blocked\",\"statuscode\":" + 203 + "}");
		}else if(empId == -5) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid credentials\",\"statuscode\":" + 204 + "}");
		}
		String id = "" + empId;
		long tokenTimeOut = TokenTimeOut.ONE_HOUR.value;
		// if valid user generating token for that user
		String token = JwtImpl.createJWT(id,tokenTimeOut);
		// if authentication is success it will return empId along with 200 status code
		// and success message.
		ObjectMapper map = new ObjectMapper();
		String jToken = null;
		try {
			jToken = map.writeValueAsString(token);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return ResponseEntity.status(HttpStatus.OK).body("{\"status\":\"login success\",\"statuscode\":" + 200
				+ ",\"empId\":" + empId + ",\"token\":" + jToken + "}");
	} // End of doAuthenticate

	/**
	 * This web resource handles request for profile of employee. It checks whether
	 * token is valid or not. If valid process the request else returns invalid
	 * user. For valid user it returns employee details as response.
	 * 
	 * @param id      is employee id who logged in.
	 * @param headers contains token and content-type details.
	 * @return ResponseEntity which contains employee details
	 */

	@GetMapping("/profileview/{employee_id}")
	public ResponseEntity<String> getById(@PathVariable("employee_id") int id, @RequestHeader HttpHeaders headers) {
		String token = "";

		EmployeeDetails emp = null;

		// getting token from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Header desn't contain token returning invalid user
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// Assigning authentication token to variable
		token = hList.get(0);

		// if given token is valid allowing user to perform further operation
		if (JwtImpl.isValidUser(id, token)) {
			// calling find employee by id service method

			emp = employeeService.findById(id);

			// checking employee is present in database or not
			if (emp != null) {

				// converting java object into JSON format
				ObjectMapper map = new ObjectMapper();
				String employee = null;
				try {
					employee = map.writeValueAsString(emp);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}

				// if employee found sending Employee as JSON
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"employee details found\",\"statuscode\":" + 200 + ",\"employee\":"
								+ employee + "}");
				// Employee not found
			} else {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"employee details Not found\",\"statuscode\":" + 201 + "}");
			}

			// if given token is invalid
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	} // End of getById

	/**
	 * This web resource handles the appraisal request from client. For valid user
	 * it process the request. If already requested for appraisal previously returns
	 * status that already requested else returns requested successfully.
	 * 
	 * @param Appraisal information
	 * @param headers   contains token and content-type
	 * @return ResponseEntity which contains requested information status and status
	 *         code.
	 */
	@PostMapping("/appraisal")
	public ResponseEntity<?> requestAppraisal(@RequestBody AppraisalRequest request,
			@RequestHeader HttpHeaders headers) {

		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);

		// getting employee id from JSON object to verify token
		int id = request.getEmployeeId();

		// return true if token is a valid and allowing to do further task
		if (JwtImpl.isValidUser(id, token)) {

			// todays date to save appraisal request
			request.setRequestedDate(new Date());

			// returning save appraisal object
			AppraisalRequest req = employeeService.request(request);

			// if already requested appraisal then returning 201
			if (req == null) {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"Already Requested \",\"statuscode\":" + 202 + "}");
			} else {
				// Converting java object into JSON format
				ObjectMapper map = new ObjectMapper();
				String appReq = null;
				try {
					appReq = map.writeValueAsString(req);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}

				// returning JSON response to client
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"Successfully Requested\",\"statuscode\":" + 200 + ",\"appraisal\":"
								+ appReq + "}");
			}
			// if given token is invalid
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	} // End of requestAppraisal

	/**
	 * This web resource handles the request form client for appraisal approved
	 * status. For valid user it process the request. After successfully appraisal
	 * request, Manager Can update employee appraisal status which comes under
	 * administration role. It returns response including appraisal approved object.
	 * 
	 * @param employee id for token verification
	 * @param headers  contains token and content-type
	 * @return ResponseEntity which contains appraisal approved information.
	 */
	@GetMapping("/appraisal/approved/{employee_id}")
	public ResponseEntity<?> fetchAppraisal(@PathVariable("employee_id") int id, @RequestHeader HttpHeaders headers) {

		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);

		// return true if token is a valid and allowing to do further task
		if (JwtImpl.isValidUser(id, token)) {

			// getting appraisal approved information form database
			AppraisalApproved req = employeeService.getApproved(id);

			// if information not found returning Record not found message
			if (req == null) {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"Record Not Found\",\"statuscode\":" + 201 + "}");
				// if record found in database then converting java object into JSON format
			} else {
				ObjectMapper map = new ObjectMapper();
				String appReq = null;
				try {
					appReq = map.writeValueAsString(req);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}

				// finally returning appraisal information as response
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"Successfully Requested\",\"statuscode\":" + 200 + ",\"appraisal\":"
								+ appReq + "}");
			}

			// if given token is invalid
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	}

	/**
	 * This web resource handles request from client for search the the employee
	 * based on the employee_id. For valid user it will process the request. It will
	 * return employee details based on employee id in the response entity object.
	 * If there is no employee with that id it will return status like no employee
	 * exist.
	 * 
	 * @param id       is employee id for token verification
	 * @param searchId for employee search with that id
	 * @param headers  contains token and content-type
	 * @return ResponseEntity including status,status code and result
	 */
	@GetMapping("/searchemployee/{employee_id}/{search_id}")
	@ResponseBody
	public ResponseEntity<?> getAll(@PathVariable("employee_id") int id, @PathVariable("search_id") String searchId,
			@RequestHeader HttpHeaders headers) {

		String token = "";

		FindEmployee entityList = null;

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// creating map to store employee information
		Map<String, Object> list = new HashMap<>();

		ObjectMapper mapper = new ObjectMapper();

		String entityJson = null;

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);

		// return true if token is a valid and allowing to do further task
		if (JwtImpl.isValidUser(id, token)) {
			// getting employee from database

			if (validations.employeeIdValidator(searchId)) {
				int serach_id = Integer.parseInt(searchId);
				entityList = (FindEmployee) employeeService.searchById(serach_id);

				// if employee with given id not present in database
				if (entityList != null) {

					list.put("statuscode", 200);
					list.put("result", entityList);
					try {
						// converting java object into JSON format
						entityJson = mapper.writeValueAsString(list);
					} catch (JsonProcessingException e) {
						e.printStackTrace();

					}

					return ResponseEntity.status(HttpStatus.OK).body(entityJson);
					// .body("{\"status\":\"Employee doesn't exist\",\"statuscode\":" + + "}");
				} else {
					// adding response information into map
					list.put("status", "No employee is exist");
					list.put("statuscode", 204);
					try {
						entityJson = mapper.writeValueAsString(list);
					} catch (JsonProcessingException e) {
						e.printStackTrace();

					}
					// finally returning responce
					return ResponseEntity.status(HttpStatus.OK).body(entityJson);
				}
			} else {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"Invalid EmployeeId\",\"statuscode\":" + 202 + "}");
			}

		}

		else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	}

	/**
	 * This web resource handles the request from client for successfulProjects of
	 * organization. For valid user it will process the request. It returns response
	 * which includes all successful projects list.
	 * 
	 * @param id      is employee id for token verification
	 * @param headers contains token and content-type information
	 * @return ResponseEntity which contains status, status code and result.
	 */
	@GetMapping("/successfullProjects/{employee_id}")
	public ResponseEntity<?> getAllSuccessfullProjects(@PathVariable("employee_id") int id,
			@RequestHeader HttpHeaders headers) {
		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);

		if (JwtImpl.isValidUser(id, token)) {
			List<SuccessfullProjects> projectsList = employeeService.getAllSuccessfullProjects();
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponce = null;
			Map<String, Object> projectsMap = new HashMap<>();

			if (projectsList == null) {
				return new ResponseEntity<List<SuccessfullProjects>>(HttpStatus.NO_CONTENT);
			}

			projectsMap.put("status", 200);
			projectsMap.put("Projects List", projectsList);

			try {
				jsonResponce = mapper.writeValueAsString(projectsMap);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

			return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	}

	/**
	 * This web resource handles the request to get all upcoming drives. For valid
	 * user it will process the request. If there are upcoming drives it will return
	 * response including drives. if there are no drives it will return status like
	 * no upcoming drives.
	 * 
	 * @param id      employee id for token verification.
	 * @param headers contains token and content-type information.
	 * @return ResponseEntity which includes status, status code and result.
	 */
	@GetMapping("/drive/{employee_id}")
	public ResponseEntity<?> getDerives(@PathVariable("employee_id") int id, @RequestHeader HttpHeaders headers) {
		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);

		if (JwtImpl.isValidUser(id, token)) {
			// Getting drives from database
			List<Drives> drive = employeeService.getAllDrives();

			// if drive not found in database
			// Returning no drive found with status code
			if (drive.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"No Upcomming Drives\",\"statuscode\":" + 201 + "}");
			} else {
				ObjectMapper map = new ObjectMapper();
				String driveList = null;
				try {
					driveList = map.writeValueAsString(drive);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"employee details found\",\"statuscode\":" + 200 + ",\"drives\":"
								+ driveList + "}");
			}
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"No Upcomming Drives\",\"statuscode\":" + 201 + "}");
		}
	}

	/**
	 * This web resource handles the request for searching employee by name. For
	 * valid user request will process. It returns employee details if employee
	 * exist with that name. Else returns status like no employee exist with that
	 * name.
	 * 
	 * @param id            for token verification
	 * @param employee_name for searching with this name
	 * @param headers       contains token and content-type information
	 * @return ResponseEntity which includes status, status code and result.
	 */
	@GetMapping("/searchemployeebyname/{employee_id}/{employee_name}")
	@ResponseBody
	public ResponseEntity<?> searchByName(@PathVariable("employee_id") int id,
			@PathVariable("employee_name") String employee_name, @RequestHeader HttpHeaders headers) {

		String token = "";

		FindEmployee entityList = null;

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// creating map to store employee information
		Map<String, Object> list = new HashMap<>();

		ObjectMapper mapper = new ObjectMapper();

		String entityJson = null;
		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);

		if (JwtImpl.isValidUser(id, token)) {
			// searching employee in databse as per given name

			if (validations.employeeNameValidator(employee_name)) {

				entityList = (FindEmployee) employeeService.searchByName(employee_name);

				// if employee not present in database
				if (entityList != null) {
					list.put("statuscode", 200);
					list.put("result", entityList);
					try {
						entityJson = mapper.writeValueAsString(list);
					} catch (JsonProcessingException e) {
						e.printStackTrace();

					}
					// returning searched employee
					return ResponseEntity.status(HttpStatus.OK).body(entityJson);
					// .body("{\"status\":\"Employee doesn't exist\",\"statuscode\":" + + "}");
				} else {
					list.put("status", "No employee is exist");
					list.put("statuscode", 204);
					try {
						entityJson = mapper.writeValueAsString(list);
					} catch (JsonProcessingException e) {
						e.printStackTrace();

					}
					// if employee present in database
					return ResponseEntity.status(HttpStatus.OK).body(entityJson);
				}

			} else {

				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"Invalid UserName\",\"statuscode\":" + 202 + "}");
			}

		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	}

	/**
	 * This web resource handles the request for latest notification. For valid user
	 * it will process the request. It will returns the latest notification among
	 * drives and rave. If there are no notifications, it will return status like no
	 * notifications found.
	 * 
	 * @param id      for token verification
	 * @param headers contains token and content-type information
	 * @return ResponseEntity which includes status, status code and result.
	 */
	@GetMapping("/latestnotification/{employeeId}")
	public ResponseEntity<?> getLastNotification(@PathVariable("employeeId") int id,
			@RequestHeader HttpHeaders headers) {

		// getting latest notification from database based on date
		Object latest_notification = employeeService.getLatestNotification();

		ObjectMapper mapper = new ObjectMapper();

		String jsonResponce = null;
		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);

		if (JwtImpl.isValidUser(id, token)) {
			// creating map to store employee information
			Map<String, Object> projectsMap = new HashMap<>();
			if (latest_notification == null) {

				projectsMap.put("status", "No notification");
				projectsMap.put("statuscode", 204);
				try {
					jsonResponce = mapper.writeValueAsString(projectsMap);
				} catch (JsonProcessingException e) {
					e.printStackTrace();

				}
				// if notification present in database
				return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);
			}

			else {
				// returning latest notification from database as JSON format
				projectsMap.put("status", 200);
				projectsMap.put("Notification", latest_notification);

				try {
					jsonResponce = mapper.writeValueAsString(projectsMap);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}

				// returning notification
				return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);

			}
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	}

	/**
	 * This web resource handles the request for updating profile. For valid user it
	 * will process the request. It updates the profile details of employee which
	 * can update. if update success it will return status like update success else
	 * no update.
	 * 
	 * @param empdetails contains fields which can update and employee id for token
	 *                   verification
	 * @param headers    contains token and content-type information
	 * @return ResponseEntity which includes status, status code and result.
	 */
	@PutMapping("/updateprofile")
	public ResponseEntity<?> updateProfile(@RequestBody EmployeeDetails empdetails,
			@RequestHeader HttpHeaders headers) {

		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);
		int id = empdetails.getEmployeeId();
		if (JwtImpl.isValidUser(id, token)) {
			
			EmployeeDetails modifiedEmployeeObject = employeeService.updateProfile(empdetails);

			ObjectMapper mapper = new ObjectMapper();

			String jsonResponce = null;

			Map<String, Object> projectsMap = new HashMap<>();

			if (modifiedEmployeeObject == null) {
				projectsMap.put("status", "No Updations done");
				projectsMap.put("statuscode", 204);
				try {
					jsonResponce = mapper.writeValueAsString(projectsMap);
				} catch (JsonProcessingException e) {
					e.printStackTrace();

				}
				// if employee doesn"t perform any updations
				return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);

			} else {

				projectsMap.put("status", 200);
				projectsMap.put("updated", modifiedEmployeeObject);

				try {
					jsonResponce = mapper.writeValueAsString(projectsMap);
				} catch (JsonProcessingException e) {

					e.printStackTrace();
				}

				// returning updated Employee object
				return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);

			}
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	}

	/**
	 * This web resource handles the change password request. For valid user it will
	 * process the request. if change password success it will return status like
	 * password updated.
	 * 
	 * @param id                   for token verification
	 * @param password             current password
	 * @param new_password
	 * @param confirm_new_password
	 * @param headers              contains token and content-type information
	 * @return ResponseEntity
	 */
	@PutMapping("/changepassword/{employeeId}/{password}/{new_password}/{confirm_new_password}")
	public ResponseEntity<?> changePassword(@PathVariable("employeeId") int id,
			@PathVariable("password") String password, @PathVariable("new_password") String new_password,
			@PathVariable("confirm_new_password") String confirm_new_password, @RequestHeader HttpHeaders headers) {

		String token = "";

		ObjectMapper mapper = new ObjectMapper();

		String jsonResponce = null;

		LoginCredentials loginCredentials = null;

		Map<String, Object> projectsMap = new HashMap<>();
		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

		token = hList.get(0);

		if (JwtImpl.isValidUser(id, token)) {

			loginCredentials = employeeService.searchByLoginId(id);
         if(validations.passwordValidator(password) && validations.passwordValidator(new_password) && validations.passwordValidator(confirm_new_password)) {
        	 if (loginCredentials.getPassword().equals(password)) {
 				if (new_password.equals(confirm_new_password)) {

 					loginCredentials.setPassword(new_password);
 					employeeService.updatePassword(loginCredentials);
 					projectsMap.put("status", 200);
 					projectsMap.put("password_updated", loginCredentials.getPassword());

 					try {
 						jsonResponce = mapper.writeValueAsString(projectsMap);
 					} catch (JsonProcessingException e) {
 						e.printStackTrace();
 					}
 					return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);
 				} else {
 					projectsMap.put("status", "new password and confirm password not matched");
 					projectsMap.put("statuscode", 204);
 					try {
 						jsonResponce = mapper.writeValueAsString(projectsMap);
 					} catch (JsonProcessingException e) {
 						e.printStackTrace();

 					}
 					return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);
 				}

 			} else {

 				projectsMap.put("status", "password you entered is wrong or invalid");
 				projectsMap.put("statuscode", 204);
 				try {
 					jsonResponce = mapper.writeValueAsString(projectsMap);
 				} catch (JsonProcessingException e) {
 					e.printStackTrace();

 				}
 				return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);

 			}
         }
         else {
        	 return ResponseEntity.status(HttpStatus.OK)
 					.body("{\"status\":\"password must starts with Capital, in between 10 -25 characters, and contains atleast special character and digit,\",\"statuscode\":" + 201 + "}");
         }

		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	} // End of changePassword

	/**
	 * This web resource handles the upload photo request from client. For valid
	 * user it will process the request.
	 * 
	 * @param id   for token verification
	 * @param file contains photo in binary format
	 * @return ResponseEntity
	 * @throws IOException
	 */
//	@PutMapping("/uploadphoto/{employee_id}")
//	public ResponseEntity<?> updatePhoto(@PathVariable("employee_id") int id, @RequestBody MultipartFile file) {
//
//		ObjectMapper mapper = new ObjectMapper();
//
//		String jsonResponce = null;
//
//		Map<String, Object> projectsMap = new HashMap<>();
//		// MultipartFile photoFile;
//		byte[] byteArr = null;
//		if (file != null) {
//			try {
//				byteArr = file.getBytes();
//			} catch (IOException e) {
//				projectsMap.put("status", "image not suppoted");
//				projectsMap.put("statuscode", 203);
//				try {
//					jsonResponce = mapper.writeValueAsString(projectsMap);
//				} catch (JsonProcessingException je) {
//
//				}
//				// if employee doesn't perform any update.
//				return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);
//			}
//		} else {
//			projectsMap.put("status", "No_Updations_done");
//			projectsMap.put("statuscode", 204);
//			try {
//				jsonResponce = mapper.writeValueAsString(projectsMap);
//			} catch (JsonProcessingException e) {
//				e.printStackTrace();
//
//			}
//			// if employee doesn't perform any update.
//			return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);
//		}
//		EmployeeDetails modifiedEmployeeObject = employeeService.uploadImage(id, byteArr);
//
//		if (modifiedEmployeeObject == null) {
//			projectsMap.put("status", "No Updations done");
//			projectsMap.put("statuscode", 204);
//			try {
//				jsonResponce = mapper.writeValueAsString(projectsMap);
//			} catch (JsonProcessingException e) {
//				e.printStackTrace();
//
//			}
//			// if employee doesn"t perform any updations
//			return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);
//
//		} else {
//
//			projectsMap.put("status", 200);
//			projectsMap.put("image_updated", modifiedEmployeeObject);
//
//			try {
//				jsonResponce = mapper.writeValueAsString(projectsMap);
//			} catch (JsonProcessingException e) {
//
//				e.printStackTrace();
//			}
//
//			// returning updated Employee object
//			return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);
//
//		}
//
//	} // End of updatePhoto
	@CrossOrigin
	@PostMapping("/uploadFile/{empId}")
	public ResponseEntity<String> uploadFile(@RequestParam MultipartFile file, @PathVariable("empId") int empId,
			@RequestHeader HttpHeaders headers) {

		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);
		if (JwtImpl.isValidUser(empId, token)) {
			String fileName = employeeService.storeFile(file, empId);

			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
					.path("/myspace/downloadprofilepic/").path(fileName).toUriString();

			ObjectMapper mapper = new ObjectMapper();

			String jsonResponce = null;

			Map<String, Object> projectsMap = new HashMap<>();

			projectsMap.put("statuscode", 200);
			projectsMap.put("status", "updated successfully");
			projectsMap.put("details",
					new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize()));
			try {
				jsonResponce = mapper.writeValueAsString(projectsMap);
			} catch (JsonProcessingException e) {

				e.printStackTrace();
			}

			return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
	} // End of uploadFile
	@CrossOrigin
	@GetMapping("/downloadprofilepic/{empId}")
	public ResponseEntity<?> downloadFile(@PathVariable("empId") int empId, HttpServletRequest request,
			@RequestHeader HttpHeaders headers) {
		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);
		if (JwtImpl.isValidUser(empId, token)) {
			// Load file as Resource
			Resource resource = employeeService.loadFileAsResource(empId);

			// Try to determine file's content type
			String contentType = null;
			try {
				contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
			} catch (IOException ex) {
				// logger.info("Could not determine file type.");
			}

			// Fallback to the default content type if type could not be determined
			if (contentType == null) {
				contentType = "application/octet-stream";
			}

			return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
					.body(resource);
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
	} // End of downloadFile
	
	/**
	 * This Resource Method Stores Username and Password in database
	 * @param login
	 * @return
	 */
	@CrossOrigin
	@PostMapping("/savelogin")
	public ResponseEntity<?> saveAuthenticateDetails(@RequestBody LoginCredentials login) {
		// calling service method
		int empId = employeeService.saveLoginCredentials(login);

		if (empId == 1) {
			// if new user
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Successfull_Saved\",\"statuscode\":" + 200 + "}");
		} else if(empId == 2){
			// if user already exits
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"username_password_already_exits\",\"statuscode\":" + 201 + "}");
		}else {
			// if user login credentials wrong
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Enter_Valid_credentials\",\"statuscode\":" + 202 + "}");
		}
	} // End of doAuthenticate
	
	@GetMapping("/logout/{employee_id}")
	public ResponseEntity<String> logout(@PathVariable("employee_id") int id, @RequestHeader HttpHeaders headers) {
		long tokenTimeOut = (5 * 1000);
		String empId = "" + id;
		String token = JwtImpl.createJWT(empId,tokenTimeOut);
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Thank You!!!\",\"statuscode\":" + 201 + "}");
	} // End of getById

}
