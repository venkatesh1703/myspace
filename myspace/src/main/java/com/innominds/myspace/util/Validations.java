package com.innominds.myspace.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class Validations {
	/*
	 * This regex will work for password validation policy. It must contains atleast
	 * one upper case, lower case, numeric digit and special characters.
	 */
	private final static String PASSWORD_PATTERN = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{10,25}$";
	/*
	 * This regex will work for username validation. It should contain lower case
	 * alphabets only and size should be greater than 2.
	 */
	private final static String USERNAME_PATTERN = "^[a-z]{3,}$";
	
	private final static String EMPLOYEE_NAME_PATTERN = "^[a-zA-Z]{1,50}$";

	private final static String MOBILE_NUMBER_PATTERN = "^(0/91)?[7-9][0-9]{9}$";

	private final static String EMAIL_PATTERN = "^[a-zA-Z0-9]+@[a-zA-Z0-9]+.com$";

	private final static String EMPID_PATTERN = "^[0-9]{5}$"; 

	/**
	 * This method validates password based on regex password pattern.
	 * 
	 * @param password
	 * @return true if input is valid else false
	 */
	public boolean passwordValidator(String password) {

		Pattern passwordPattern = Pattern.compile(PASSWORD_PATTERN);
		Matcher matchPassword = passwordPattern.matcher(password);

		return matchPassword.matches();
	}

	/**
	 * This mehtod validates username based on regex username pattern.
	 * 
	 * @param username
	 * @return true if input is valid else false
	 */
	public boolean usernameValidator(String username) {
		if (username != null) {
			Pattern usernamePattern = Pattern.compile(USERNAME_PATTERN);
			Matcher matchPassword = usernamePattern.matcher(username);

			return matchPassword.matches();
		} else {
			return false;
		}
	}

	/**
	 * This method validates mobile number
	 * 
	 * @param mobileNumber
	 * @return true if valid else false
	 */
	public boolean mobilenumberValidator(String mobileNumber) {
		if (mobileNumber != null) {
			Pattern mobileNumberPattern = Pattern.compile(MOBILE_NUMBER_PATTERN);
			Matcher matchmobileNumber = mobileNumberPattern.matcher(mobileNumber);
			return matchmobileNumber.matches();
		} else {
			return false;
		}
	}

	/**
	 * This method validates email
	 * 
	 * @param email
	 * @return true if valid else false
	 */
	public boolean emailValidator(String email) {
		if (email != null) {
			Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
			Matcher matchEmail = emailPattern.matcher(email);
			return matchEmail.matches();
		} else {
			return false;
		}
	}

	/**
	 * This method validates employee id
	 * 
	 * @param empId
	 * @return
	 */
	public boolean employeeIdValidator(String empid) {
		//String empId=""+empid;
		if (empid != null) {
			Pattern empIdPattern = Pattern.compile(EMPID_PATTERN);
			Matcher matchEmpID = empIdPattern.matcher(empid);
			return matchEmpID.matches();
		} else {
			return false;
		}
	}
	
	public boolean employeeNameValidator(String empName) {
	if(empName != null) {
		Pattern empNamePattern = Pattern.compile(EMPLOYEE_NAME_PATTERN);
		Matcher matchEmpName = empNamePattern.matcher(empName);
		return matchEmpName.matches();
	}
	else {
		return false;
	}
		
	}

}
