package com.innominds.myspace.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.innominds.myspace.dao.EmployeeDao;
import com.innominds.myspace.dao.RaveDao;
import com.innominds.myspace.entities.Rave;

/**
 * This service class contains method implementations containing processing
 * request from controller and fetching data from dao layer.
 * 
 * @author myspace-A
 *
 */
@Service
@Transactional
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	private RaveDao raveDao;

	@Autowired
	private EmployeeDao employeeDao;

	/**
	 * This method fetches all rave notifications available
	 * 
	 * @return list of Rave object
	 */
	public List<Rave> getAllRave() {

		return (List<Rave>) raveDao.findAll();
	}

	/**
	 * This method fetches all rave notifications of an employee based on employee
	 * id.
	 * 
	 * @param employeeId
	 * @return list of Rave
	 */
	public List<Rave> getAllRaveByEmpId(int empId) {

		return raveDao.findByemployeeId(empId);
	}

	/**
	 * This service method used to insert rave object into data base table.
	 * 
	 * @param employeeId
	 * @param discription
	 * @param createdBy
	 * @param raveCompliment
	 * 
	 * @return 0 for successfull insertion else -1
	 */
	@Override
	public int insertNewRave(Rave rave) {
		int status = -1;
		// if employeeId is not exist then below if block will execute
		if (!employeeDao.existsById(rave.getEmployeeId()))
			return -2;
		if (rave != null) {
			// Adding current date to rave object
			Date date = Calendar.getInstance().getTime();
			rave.setCreatedAt(date);
			Rave raveObj = raveDao.save(rave);
			status = (raveObj != null) ? 0 : -1;
		}
		return status;
	} // End of insertNewRave

	/**
	 * This method fetches all rave posted by an employee
	 * 
	 * @param employeeId
	 * 
	 * @return list of rave objects
	 */
	@Override
	public List<Rave> getAllRavePostedBy(int empId) {

		return raveDao.findBycreatedBy(empId);
	} // End of getAllRavePostedBy
}
