package com.innominds.myspace.service;

import java.util.List;

import com.innominds.myspace.entities.Answer;
import com.innominds.myspace.entities.Question;

/**
 * this is an interface for QueryForumService and implemntation is provided in
 * QueryForumImpl class
 * 
 * @author IMVIZAG
 *
 */
public interface QueryForumService {

	int deleteQuestion(int questionId);

	int postQuestion(Question question);
	
	int postAnswer(Answer answer);
	
	List<Answer> fetchAllAnswersOfQuestion(int questionId);
	
	int deleteAnswer(int answerId);
	
	List<Question>fetachAllQuestion();
	
	int updateAnswer(Answer answer);
}
