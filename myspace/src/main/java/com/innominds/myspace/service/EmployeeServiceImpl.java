package com.innominds.myspace.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.innominds.myspace.dao.AppraisalApprovedDao;
import com.innominds.myspace.dao.AppraisalRequestDao;
import com.innominds.myspace.dao.DriveDao;
import com.innominds.myspace.dao.EmployeeDao;
import com.innominds.myspace.dao.LoginCredentialsDao;
import com.innominds.myspace.dao.RaveDao;
import com.innominds.myspace.dao.SearchEmployeeDao;
import com.innominds.myspace.dao.SuccessfullProjectsDao;
import com.innominds.myspace.entities.AppraisalApproved;
import com.innominds.myspace.entities.AppraisalRequest;
import com.innominds.myspace.entities.Drives;
import com.innominds.myspace.entities.EmployeeDetails;
import com.innominds.myspace.entities.FindEmployee;
import com.innominds.myspace.entities.LoginCredentials;
import com.innominds.myspace.entities.Rave;
import com.innominds.myspace.entities.SuccessfullProjects;
import com.innominds.myspace.exception.FileStorageException;
import com.innominds.myspace.exception.MyFileNotFoundException;
import com.innominds.myspace.property.FileStorageProperties;
import com.innominds.myspace.util.PasswordEncyption;
import com.innominds.myspace.util.Validations;

/**
 * This a implementational class for EmployeeService Interface.
 * 
 * @author IMVIZAG
 *
 */
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private Validations validate;

	@Autowired
	private LoginCredentialsDao logindao;

	@Autowired
	private DriveDao driveDao;

	@Autowired
	private RaveDao raveDao;

	@Autowired
	private SuccessfullProjectsDao successfullProjectsDao;

	@Autowired
	private SearchEmployeeDao searchEmpDao;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private AppraisalRequestDao appraisalDao;

	@Autowired
	private AppraisalApprovedDao approvedDao;

	@Autowired
	private EmployeeService employeeService;

	private Path fileStorageLocation = null;
	
	public EmployeeServiceImpl() {
		// TODO Auto-generated constructor stub
	}
	

	@Autowired
	public EmployeeServiceImpl(FileStorageProperties fileStorageProperties) {
//		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
//
//		try {
//			Files.createDirectories(this.fileStorageLocation);
//		} catch (Exception ex) {
//			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",
//					ex);
//		}
	} // End of constructor

	public String storeFile(MultipartFile file, int empId) {
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}
			fileName = empId + ".jpg";
			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

			// storing file name into database
			EmployeeDetails emp = employeeDao.findById(empId).get();
			emp.setFileName(fileName);
			employeeDao.save(emp);

			return empId + "";
		} catch (IOException ex) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
		}
	} // End of storeFile

	public Resource loadFileAsResource(int empId) {
		EmployeeDetails emp = employeeDao.findById(empId).get();
		String fileName = emp.getFileName();
		try {
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new MyFileNotFoundException("File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			throw new MyFileNotFoundException("File not found " + fileName, ex);
		}
	} // End of loadFileAsResource

	/**
	 * This service method calls dao methods to retrieve records and authenticates
	 * and returns value to controller respectively.
	 * 
	 * @param username
	 * @param password
	 * 
	 * @return if both are correct it returns empID, -1 : if no user , -2 : if
	 *         password is wrnog ,
	 */
//	public int doLogin(String username, String password) {
//		// validating username before calling dao layer
//		if (!validate.usernameValidator(username)) {
//			return -1;
//		} else if (!validate.passwordValidator(password)) { // validating password before calling dao layer
//			return -2;
//		}
//		// calling dao method
//		LoginCredentials loginCredentials = logindao.findByUsername(username);
//
//		if (loginCredentials == null) {
//			// if user name wrong it returns -1
//			return -1;
//		} else if (!loginCredentials.getPassword().equals(password)) {
//			// if password wrong it returns -2
//			return -2;
//		} else {
//			// if credentials are correct it returns empId
//			return loginCredentials.getEmployeeId();
//		}
//	}

	/*
	 * 
	 * (-5) to represent invalid credentials. (-3) to represent user is blocked.
	 * (-1,-2) to represent no.of attempts remaining.
	 */
	/**
	 * This method contains service logic to perform login operation. It validates
	 * the inputs using regex. This logic limits the wrong attempts. Allows only 3
	 * wrong attempts per day, after that user gets blocked for 24 hours. On
	 * successful login employee id will return.
	 * 
	 * @return integer to represent different scenarios
	 * @param username
	 * @param password
	 */
	public int doLogin(String username, String password) {
		// validating credentials
		if (validate.usernameValidator(username) && validate.passwordValidator(password)) {
			// if valid check user name exist or not
			LoginCredentials loginCredentials = logindao.findByUsername(username);

			if (loginCredentials == null) {
				// if user name wrong means unknown user. So, it returns -5 which represents
				// invalid credentials
				return -5;
			} else {
				// check status
				if (loginCredentials.getStatus().equals("active")) {
					// if user is active, check password
					if (loginCredentials.getPassword().equals(password)) {
						// if password is correct reset attempts and login
						loginCredentials.setAttempts(0);
						// saving object after re-setting attempts
						logindao.save(loginCredentials);
						// after credentials are matched returning employeeId.
						return loginCredentials.getEmployeeId();
					} else {
						if (loginCredentials.getAttempts() == 0) {
							loginCredentials.setFirstWrongAttemptdatetime(
									new Timestamp(Calendar.getInstance().getTimeInMillis()));
						} else {
							Timestamp firstAttemptTime = loginCredentials.getFirstWrongAttemptdatetime();
							long difference = new Timestamp(Calendar.getInstance().getTimeInMillis()).getTime()
									- firstAttemptTime.getTime();
							if ((difference / 1000) >= (24 * 60 * 60)) {
								loginCredentials.setAttempts(0);
							}
						}

						// increment attempts
						loginCredentials.setAttempts(loginCredentials.getAttempts() + 1);
						if (loginCredentials.getAttempts() >= 3) {
							// if 3 attempts made block the user for 24 hours
							loginCredentials.setStatus("blocked");
							loginCredentials
									.setBlockedDateTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
							// reset attempts
							loginCredentials.setAttempts(0);
							// saving object
							logindao.save(loginCredentials);
							// returning -3 to represent that user is blocked
							return -3;
						} else {
							// if less than 3 attempts return no of attempts left
							return (loginCredentials.getAttempts() - 3);
						}
					}
				} else {
					// if user already blocked check blocked time with current time
					Timestamp blockedTime = loginCredentials.getBlockedDateTime();
					long blockedDuration = new Timestamp(Calendar.getInstance().getTimeInMillis()).getTime()
							- blockedTime.getTime();
					if ((blockedDuration / 1000) >= (24 * 60 * 60)) {
						loginCredentials.setStatus("active");
						loginCredentials.setBlockedDateTime(null);
						// check password
						if (loginCredentials.getPassword().equals(password)) {
							// if password is correct reset attempts and login
							loginCredentials.setAttempts(0);
							// saving object after re-setting attempts
							logindao.save(loginCredentials);
							// after credentials are matched returning employeeId.
							return loginCredentials.getEmployeeId();
						} else {
							if (loginCredentials.getAttempts() == 0) {
								loginCredentials.setFirstWrongAttemptdatetime(
										new Timestamp(Calendar.getInstance().getTimeInMillis()));
							} else {
								Timestamp firstAttemptTime = loginCredentials.getFirstWrongAttemptdatetime();
								long difference = new Timestamp(Calendar.getInstance().getTimeInMillis()).getTime()
										- firstAttemptTime.getTime();
								if ((difference / 1000) >= (24 * 60 * 60)) {
									loginCredentials.setAttempts(0);
								}
							}

							// increment attempts
							loginCredentials.setAttempts(loginCredentials.getAttempts() + 1);
							if (loginCredentials.getAttempts() >= 3) {
								// if 3 attempts made block the user for 24 hours
								loginCredentials.setStatus("blocked");
								loginCredentials
										.setBlockedDateTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
								// reset attempts
								loginCredentials.setAttempts(0);
								// saving object
								logindao.save(loginCredentials);
								// returning -3 to represent that user is blocked
								return -3;
							} else {
								// if less than 3 attempts return no of attempts left
								return (loginCredentials.getAttempts() - 3);
							}
						}
					}
					return -3;
				}
			}
		}
		// if credentials are not valid
		return -5;
	}

	/**
	 * service method for find the employee based on the EmployeeId
	 * 
	 * @param employeeId
	 * 
	 * @return if the employee exists it will return EmployeeDetails object
	 */
	@Override
	public EmployeeDetails findById(int employeeId) {
		EmployeeDetails emp = null;

		try {
			emp = employeeDao.findById(employeeId).get();
			return emp;
		} catch (Exception e) {
			return emp;
		}
	}

	/**
	 * Service method to search the employee based on the employee_id
	 * 
	 * @param employeeId
	 * @return if employee exists it will return employee object else it will return
	 *         null
	 */
	@Override
	public FindEmployee searchById(int employeeId) {
		// System.out.println(searchEmpDao.findById(employeeId).get());
		FindEmployee emp = null;
		try {
			EmployeeDetails edetails = searchEmpDao.findById(employeeId).get();

			emp = new FindEmployee();
			emp.setEmployee_id(edetails.getEmployeeId());
			emp.setName(edetails.getName());
			emp.setEmploymentType(edetails.getEmploymentType());
			emp.setDepartment(edetails.getDepartment());
			emp.setDesignation(edetails.getDesignation());
			emp.setPractice(edetails.getPractice());
			emp.setWorkLocation(edetails.getWorkLocation());
			emp.setPermanentLocation(edetails.getPermanentLocation());
			emp.setCompanyEmailId(edetails.getCompanyEmailId());

			return emp;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Service method for get all the Successfull Projects
	 * 
	 * @return it will return SuccessfullProjects object
	 */
	@Override
	public List<SuccessfullProjects> getAllSuccessfullProjects() {
		return (List<SuccessfullProjects>) successfullProjectsDao.findAll();

	}

	/**
	 * Serivce method for get all the Drives information
	 * 
	 * @returnand it will returns all Drives Data
	 */
	@Override
	public List<Drives> getAllDrives() {
		Date date1 = new Date();
		List<Drives> driveList = driveDao.getAllDrives(date1);
		return driveList;
	}

	/**
	 * this is a service method to request thr appraisal
	 * 
	 * @param employeeId
	 * @param            Date(i.e requested date)
	 * @return appraisal object
	 */
	@Override
	public AppraisalRequest request(AppraisalRequest request) {
		AppraisalRequest appraisal = appraisalDao.findByEmployeeId(request.getEmployeeId());
		if (appraisal == null) {
			return appraisalDao.save(request);

		} else {
			return null;
		}
	}

	/**
	 * This is a service method to check weather appraisal approved or not based on
	 * employeeId
	 * 
	 * @param employeeId
	 * @return AppraisalApproved object
	 */
	@Override
	public AppraisalApproved getApproved(int employee_id) {
		return approvedDao.findByemployeeId(employee_id);
	}

	/**
	 * Service method for serach the employee based on name and,
	 * 
	 * @param name
	 * @return if the employee exist with the name then it will returns Employee
	 *         object, else it will return null
	 */
	@Override
	public FindEmployee searchByName(String employee_name) {
		FindEmployee emp = null;
		try {
			EmployeeDetails edetails = searchEmpDao.findByname(employee_name);

			emp = new FindEmployee();
			emp.setEmployee_id(edetails.getEmployeeId());
			emp.setName(edetails.getName());
			emp.setEmploymentType(edetails.getEmploymentType());
			emp.setDepartment(edetails.getDepartment());
			emp.setDesignation(edetails.getDesignation());
			emp.setPractice(edetails.getPractice());
			emp.setWorkLocation(edetails.getWorkLocation());
			emp.setPermanentLocation(edetails.getPermanentLocation());
			emp.setCompanyEmailId(edetails.getCompanyEmailId());

			return emp;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * service method for getting the last uploaded drive from the database and it
	 * will Drive object
	 * 
	 * @return Drive object
	 */
	@Override
	public Drives getLastIndexDrive() {

		Drives drive = null;

		try {

			long lastIndex = driveDao.count();

			drive = driveDao.findById((int) lastIndex).get();

			return drive;
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * This a service method to getting the last updated Rave and it will return-
	 * Rave object
	 * 
	 * @return Rave object
	 */
	@Override
	public Rave getLastIndexRave() {
		Rave rave = null;

		try {

			long lastindex = raveDao.count();

			rave = raveDao.findById((int) lastindex).get();

			return rave;
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * This is a service method to get ONE latest notification from the Drive table-
	 * and Rave table
	 * 
	 * @return it will return any of the object, either Rave or Drive
	 */
	public Object getLatestNotification() {

		Drives drive = employeeService.getLastIndexDrive();

		Rave rave = employeeService.getLastIndexRave();

		Date drive_date = null;

		Date rave_date = null;
		Drives drive_notification = null;

		Rave rave_notification = null;

		if (drive != null && rave != null) {
			drive_date = (Date) drive.getDriveDate();

			rave_date = rave.getCreatedAt();

			if (drive_date.compareTo(rave_date) > 0) {
				drive_notification = drive;
				return drive_notification;

			} else if (drive_date.compareTo(rave_date) < 0) {
				rave_notification = rave;
				return rave_notification;

			} else if (drive_date.compareTo(rave_date) == 0) {
				drive_notification = drive;
				return drive_notification;

			} else {
				return null;
			}
		} else if (drive == null) {
			// rave_date = rave.getCreatedAt();
			rave_notification = rave;
			return rave_notification;
		} else {
			drive_notification = drive;
			return drive_notification;
		}

	}

	/**
	 * This method updates the employee details which reflects in database.
	 * 
	 * @param personal_email_id
	 * @param business_email_id
	 * @param primary_mobile_number
	 * @param alternative_mobile_number
	 * @param workPlacePhoneNumber
	 * @param skpe_id
	 * 
	 * @return EmployeeDetails object
	 */
	@Override
	public EmployeeDetails updateProfile(EmployeeDetails empdetails) {
		EmployeeDetails employeedetails = null;
		boolean isUpdated = false;
		if (empdetails != null) {
			// setting updatable fields to employee details object
			employeedetails = employeeDao.findById(empdetails.getEmployeeId()).get();
			

				if (empdetails.getPersonalEmailId() != null&&isUpdated==false) {
					isUpdated = empdetails.getPersonalEmailId().equals(employeedetails.getPersonalEmailId()) ? false
							: true;
				}

				employeedetails.setPersonalEmailId(empdetails.getPersonalEmailId());
	
			
				if (empdetails.getBusinessEmailId() != null&&isUpdated==false) {
					isUpdated = empdetails.getBusinessEmailId().equals(employeedetails.getBusinessEmailId()) ? false
							: true;
				}

				employeedetails.setBusinessEmailId(empdetails.getBusinessEmailId());
			
			
				if (empdetails.getPrimaryMobileNumber() != null&&isUpdated==false) {
					isUpdated = empdetails.getPrimaryMobileNumber().equals(employeedetails.getPrimaryMobileNumber())
							? false
							: true;
				}
				employeedetails.setPrimaryMobileNumber(empdetails.getPrimaryMobileNumber());
			
			
				if (empdetails.getAlternativeMobileNumber() != null&&isUpdated==false) {
					isUpdated = empdetails.getAlternativeMobileNumber()
							.equals(employeedetails.getAlternativeMobileNumber()) ? false : true;
				}
				employeedetails.setAlternativeMobileNumber(empdetails.getAlternativeMobileNumber());
			
			
				if (empdetails.getWorkPlacePhoneNumber() != null&&isUpdated==false) {
					isUpdated = empdetails.getWorkPlacePhoneNumber().equals(employeedetails.getWorkPlacePhoneNumber())
							? false
							: true;
				}
				employeedetails.setWorkPlacePhoneNumber(empdetails.getWorkPlacePhoneNumber());
			
			if (empdetails.getSkypeId() != null&&isUpdated==false)
				isUpdated = empdetails.getSkypeId().equals(employeedetails.getSkypeId()) ? false : true;
			employeedetails.setSkypeId(empdetails.getSkypeId());

			// save will work as update
			return isUpdated ? employeeDao.save(employeedetails) : null;
		} else {
			return null;
		}
	} // End of updateProfile

	/**
	 * this is a service method to change the password
	 * 
	 * @param
	 * @return
	 */
	@Override
	public void updatePassword(LoginCredentials loginCredentials) {
		logindao.save(loginCredentials);

	}

	/**
	 * this is a serivice method to search the password whether it exists in the
	 * database or not
	 * 
	 * @param password
	 * @return LoginCredentials
	 */
	@Override
	public LoginCredentials searchByLoginId(int employeeId) {

		LoginCredentials loginCredentials = logindao.findByid(employeeId);
		// System.out.println(loginCredentials.getPassword());
		return loginCredentials;
	}

	@Override
	public int saveLoginCredentials(LoginCredentials login) {
		String username = login.getUsername();
		String pass = login.getPassword();

		if (new Validations().usernameValidator(username) && new Validations().passwordValidator(pass)) {
			LoginCredentials loginCredentials = logindao.findByUsername(username);
			if (loginCredentials == null) {
				String encryPassword = new PasswordEncyption().getEncryptedPassword(pass);
				login.setUsername(username);
				login.setPassword(encryPassword);
				logindao.save(login);
				return 1;
			} else {
				return 2;
			}
		} else {
			return 3;
		}

	}

//	@Override
//	public EmployeeDetails uploadImage(int id, byte[] photo) {
//		EmployeeDetails emp = employeeDao.findById(id).get();
//		emp.setPhoto(photo);
//		employeeDao.save(emp);
//		return emp;
//	}

}
