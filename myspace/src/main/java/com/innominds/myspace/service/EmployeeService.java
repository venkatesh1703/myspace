package com.innominds.myspace.service;

import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.innominds.myspace.entities.AppraisalApproved;
import com.innominds.myspace.entities.AppraisalRequest;
import com.innominds.myspace.entities.Drives;
import com.innominds.myspace.entities.EmployeeDetails;
import com.innominds.myspace.entities.FindEmployee;
import com.innominds.myspace.entities.LoginCredentials;
import com.innominds.myspace.entities.Rave;
import com.innominds.myspace.entities.SuccessfullProjects;

/**
 * This is a service interface, it having the following methods, whose
 * implemtation is provided in EmployeeServiceImpl class
 * 
 * @author IMVIZAG
 *
 */
public interface EmployeeService {
	/**
	 * This abstract method gets employee from databse as per given id
	 * 
	 * @param emmployeeId
	 * @return Employee object
	 */
	int doLogin(String username, String password);

	public EmployeeDetails findById(int emmployeeId);

	public List<Drives> getAllDrives();

	public FindEmployee searchById(int employee_id);

	public AppraisalRequest request(AppraisalRequest request);

	public AppraisalApproved getApproved(int employee_id);

	public FindEmployee searchByName(String employee_name);

	public List<SuccessfullProjects> getAllSuccessfullProjects();

	public Drives getLastIndexDrive();

	public Rave getLastIndexRave();

	public Object getLatestNotification();

	public LoginCredentials searchByLoginId(int employeeId);

	public EmployeeDetails updateProfile(EmployeeDetails empdetails);

	public void updatePassword(LoginCredentials loginCredentials);

//	EmployeeDetails uploadImage(int id, byte[] photo);
	
	String storeFile(MultipartFile file, int empId);
	
	Resource loadFileAsResource(int empId);
	
	int saveLoginCredentials(LoginCredentials login);
	
}
