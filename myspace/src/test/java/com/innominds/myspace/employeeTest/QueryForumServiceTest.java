package com.innominds.myspace.employeeTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.innominds.myspace.dao.AnswerDao;
import com.innominds.myspace.dao.QuestionDao;
import com.innominds.myspace.entities.Answer;
import com.innominds.myspace.entities.Question;
import com.innominds.myspace.service.QueryForumServiceImpl;

/**
 * This test class contains JUnit test cases for QueryForumServiceImpl class
 * methods. For Mocking we used Mockito here.
 * 
 * @author IMVIZAG
 *
 */
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class QueryForumServiceTest {
	@Mock
	QuestionDao questionDao;

	@Mock
	AnswerDao answerDao;

	@InjectMocks
	QueryForumServiceImpl queryService;

	/**
	 * This test case is to Test fetachAllQuestion method of QueryForumServiceImpl
	 * class for one record.
	 */
	@Test
	public void fetachAllQuestionTest_forOneRecord() {
		List<Question> qlist = new ArrayList<>();
		// adding question object to list
		qlist.add(new Question(1, 10354, "what is algoritham?", Calendar.getInstance().getTime()));
		when(questionDao.findAll()).thenReturn(qlist);
		assertTrue(queryService.fetachAllQuestion().size() == 1);
	}

	/**
	 * This test case is to Test fetachAllQuestion method of QueryForumServiceImpl
	 * class for list containing multiple records.
	 */
	@Test
	public void fetachAllQuestionTest_forMultipleRecords() {
		List<Question> qlist = new ArrayList<>();
		// adding question objects to list
		qlist.add(new Question(1, 10354, "what is algoritham?", Calendar.getInstance().getTime()));
		qlist.add(new Question(2, 10343, "what is algoritham?", Calendar.getInstance().getTime()));
		when(questionDao.findAll()).thenReturn(qlist);
		assertTrue(queryService.fetachAllQuestion().size() > 1);
	}

	/**
	 * This test case is to Test fetachAllQuestion method of QueryForumServiceImpl
	 * class for list containing no records.
	 */
	@Test
	public void fetachAllQuestionTest_forNoRecords() {
		// creating empty array list
		List<Question> qlist = new ArrayList<>();
		when(questionDao.findAll()).thenReturn(qlist);
		assertTrue(queryService.fetachAllQuestion().isEmpty());
	}

	/**
	 * This test case is to Test fetchAllAnswersOfQuestion method of
	 * QueryForumServiceImpl class for list containing multiple records.
	 */
	@Test
	public void fetchAllAnswersOfQuestionTest_forMultipleRecords() {
		List<Answer> answers = new ArrayList<>();
		// adding answer objects to the list
		answers.add(new Answer(1, 10354, "algoritham is step by step process of programm exectution.",
				Calendar.getInstance().getTime(), 1));
		answers.add(new Answer(2, 10343, "algoritham is step by step process of programm exectution.",
				Calendar.getInstance().getTime(), 1));
		when(answerDao.findAllByQuestionId(1)).thenReturn(answers);
		assertTrue(queryService.fetchAllAnswersOfQuestion(1).size() > 1);
	}

	/**
	 * This test case is to Test fetchAllAnswersOfQuestion method of
	 * QueryForumServiceImpl class for list containing no records.
	 */
	@Test
	public void fetchAllAnswersOfQuestionTest_forNoRecords() {
		List<Answer> answers = new ArrayList<>();
		when(answerDao.findAllByQuestionId(1)).thenReturn(answers);
		assertTrue(queryService.fetchAllAnswersOfQuestion(1).isEmpty());
	}

	/**
	 * This test case is to test deleteQuestion method of QueryForumServiceImpl
	 * class for positive case.
	 */
	@Test
	public void deleteQuestionTest_positiveCase() {
		when(questionDao.existsById(1)).thenReturn(true);
		assertEquals(0, queryService.deleteQuestion(1));
	}

	/**
	 * This test case is to test deleteQuestion method of QueryForumServiceImpl
	 * class for negative case.
	 */
	@Test
	public void deleteQuestionTest_negativeCase() {
		when(questionDao.existsById(1)).thenReturn(false);
		assertEquals(-1, queryService.deleteQuestion(1));
	}

	/**
	 * This test case is to test postQuestion method of QueryForumServiceImpl class
	 * for positive case.
	 */
	@Test
	public void postQuestionTest_positiveCase() {
		Question q = new Question(1, 10354, "what is algorithm?", Calendar.getInstance().getTime());
		when(questionDao.save(q)).thenReturn(q);
		assertEquals(0, queryService.postQuestion(q));
	}

	/**
	 * This test case is to test postQuestion method of QueryForumServiceImpl class
	 * for negative case.
	 */
	@Test
	public void postQuestionTest_negativeCase() {
		Question q = new Question(1, 10354, "what is algorithm?", Calendar.getInstance().getTime());
		when(questionDao.save(q)).thenReturn(null);
		assertEquals(-1, queryService.postQuestion(q));
	}

	/**
	 * This test case is to test postAnswer method of QueryForumServiceImpl class
	 * for positive case.
	 */
	@Test
	public void postAnswerTest_positiveCase() {
		Answer answer = new Answer(1, 10354, "algorithm is step by step process of progremm execution.",
				Calendar.getInstance().getTime(), 1);
		when(questionDao.existsById(answer.getQuestionId())).thenReturn(true);
		when(answerDao.save(answer)).thenReturn(answer);
		assertEquals(0, queryService.postAnswer(answer));
	}

	/**
	 * This test case is to test postAnswer method of QueryForumServiceImpl class
	 * for negative case.
	 */
	@Test
	public void postAnswerTest_negativeCase() {
		Answer answer = new Answer(1, 10354, "algorithm is step by step process of progremm execution.",
				Calendar.getInstance().getTime(), 1);
		// if question it self does not exist it will return false as below.
		when(questionDao.existsById(answer.getQuestionId())).thenReturn(false);
		assertEquals(-1, queryService.postAnswer(answer));
	}

	/**
	 * This test case is to test deleteAnswer method of QueryForumServiceImpl class
	 * for positive case.
	 */
	@Test
	public void deleteAnswerTest_positiveCase() {
		when(answerDao.existsById(1)).thenReturn(true);
		assertEquals(0, queryService.deleteAnswer(1));
	}

	/**
	 * This test case is to test deleteAnswer method of QueryForumServiceImpl class
	 * for negative case.
	 */
	@Test
	public void deleteAnswerTest_negativeCase() {
		when(answerDao.existsById(1)).thenReturn(false);
		assertEquals(-1, queryService.deleteAnswer(1));
	}

	/**
	 * This test case is to test updateAnswer method of QueryForumServiceImpl class
	 * for positive case.
	 */
	@Test
	public void updateAnswer_positiveCase() {
		Answer answer = new Answer(1, 10354, "java is a programming language.", Calendar.getInstance().getTime(), 1);
		Answer existingAnswer = new Answer(1, 10354, "java is object oriented language.",
				Calendar.getInstance().getTime(), 1);
		when(answerDao.findById(answer.getAnswerId())).thenReturn(Optional.of(existingAnswer));
		when(answerDao.save(answer)).thenReturn(answer);
		assertEquals(0, queryService.updateAnswer(answer));
	}

	/**
	 * This test case is to test updateAnswer method of QueryForumServiceImpl class
	 * for negative case(i.e if answer object is null).
	 */
	@Test
	public void updateAnswer_negativeCase1() {
		Answer answer = null;
		assertEquals(-1, queryService.updateAnswer(answer));
	}

	/**
	 * This test case is to test updateAnswer method of QueryForumServiceImpl class
	 * for negative case(i.e if answer is not modified).
	 */
	@Test
	public void updateAnswer_negativeCase2() {
		Answer answer = new Answer(1, 10354, "java is a programming language.", Calendar.getInstance().getTime(), 1);
		Answer existingAnswer = new Answer(1, 10354, "java is a programming language.",
				Calendar.getInstance().getTime(), 1);
		when(answerDao.findById(answer.getAnswerId())).thenReturn(Optional.of(existingAnswer));
		assertEquals(-2, queryService.updateAnswer(answer));
	}

}
