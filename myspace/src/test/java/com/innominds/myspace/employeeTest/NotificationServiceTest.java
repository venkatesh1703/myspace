package com.innominds.myspace.employeeTest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.innominds.myspace.dao.EmployeeDao;
import com.innominds.myspace.dao.RaveDao;
import com.innominds.myspace.entities.Rave;
import com.innominds.myspace.service.NotificationServiceImpl;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class NotificationServiceTest {

	@InjectMocks
	private NotificationServiceImpl notificationServiceImpl;

	@Mock
	private RaveDao raveDao;
	@Mock
	EmployeeDao empDao;

	/**
	 * Test case for getting all the rave
	 */
	@Test
	public void getAllRave() {

		List<Rave> raveList = new ArrayList<Rave>();
		Rave rave = new Rave();
		rave.setCreatedBy(10371);
		rave.setRaveCompliment("Good Job");
		rave.setEmployeeId(10353);

		Rave rave1 = new Rave();
		rave1.setCreatedBy(10371);
		rave1.setRaveCompliment("Good Job");
		rave1.setEmployeeId(10352);

		raveList.add(rave);
		raveList.add(rave1);

		when(raveDao.findAll()).thenReturn(raveList);

		assertEquals(2, notificationServiceImpl.getAllRave().size());

		// assertEquals(7, notificationServiceImpl.getAllRave().size());

	}

	/**
	 * Testcase for search all the raves for the individual employee
	 */
	@Test
	public void getAllRaveByEmpId() {

		List<Rave> raveList = new ArrayList<Rave>();
		Rave rave = new Rave();
		rave.setCreatedBy(10371);
		rave.setRaveCompliment("Good Job");
		rave.setEmployeeId(10352);

		Rave rave1 = new Rave();
		rave1.setCreatedBy(10371);
		rave1.setRaveCompliment("Good Job");
		rave1.setEmployeeId(10352);

		Rave rave2 = new Rave();
		rave1.setCreatedBy(10371);
		rave1.setRaveCompliment("Good Job");
		rave1.setEmployeeId(10352);

		raveList.add(rave);
		raveList.add(rave1);
		raveList.add(rave2);

		when(raveDao.findByemployeeId(10352)).thenReturn(raveList);

		assertEquals(3, notificationServiceImpl.getAllRaveByEmpId(10352).size());

		// assertEquals(30, notificationServiceImpl.getAllRaveByEmpId(10352).size());

	}

	/**
	 * Test case for get all the raves posted by the employee
	 */
	@Test
	public void getAllRavePostedBy() {
		List<Rave> raveList = new ArrayList<Rave>();

		Rave rave = new Rave();
		rave.setCreatedBy(10371);
		rave.setRaveCompliment("Good Job");
		rave.setEmployeeId(10352);

		Rave rave1 = new Rave();
		rave.setCreatedBy(10371);
		rave.setRaveCompliment("Good Job");
		rave.setEmployeeId(10352);

		raveList.add(rave);
		raveList.add(rave1);

		when(raveDao.findBycreatedBy(10371)).thenReturn(raveList);

		assertEquals(2, notificationServiceImpl.getAllRavePostedBy(10371).size());

		// assertEquals(21, notificationServiceImpl.getAllRavePostedBy(10371).size());

	}

	/**
	 * Test case for post the new rave
	 */
	@Test
	public void insertNewRave() {
		java.util.Date date = new java.util.Date();
		Rave rave = new Rave();

		rave.setCreatedAt(date);
		
		System.out.println(date);
		rave.setCreatedBy(10377);
		rave.setEmployeeId(10351);
		//List<Rave> raveList = new ArrayList<Rave>();
		//raveList.add(rave);
		rave.setDiscription("Great Work");
		when(empDao.existsById(10351)).thenReturn(true);
		when(raveDao.save(rave)).thenReturn(rave);
		assertEquals(0, notificationServiceImpl.insertNewRave(rave));
	}
	
	
}
